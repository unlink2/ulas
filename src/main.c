#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "ulas.h"
#include <unistd.h>
#include <getopt.h>

/**
 * TODO: Write documentation
 * FIXME: it is not possible to use #define variables in macros 
 */


void ulas_getopt(int argc, char **argv, struct ulas_config *cfg) {
  int warnings[255];
  memset(warnings, 0, 255 * sizeof(int));
  warnings['a'] = ULAS_WARN_ALL;
  warnings['o'] = ULAS_WARN_OVERFLOW;

  int c = 0;
  while ((c = getopt(argc, argv, ULAS_OPTS ULAS_OPTS_ARG)) != -1) {
    switch (c) {
    case 'h':
      ulas_help();
      exit(0);
      break;
    case 'V':
      ulas_version();
      exit(0);
      break;
    case 'v':
      cfg->verbose = 1;
      break;
    case 'o':
      cfg->output_path = strndup(optarg, ULAS_PATHMAX);
      break;
    case 'p':
      cfg->preproc_only = 1;
      break;
    case 's':
      cfg->sym_path = strndup(optarg, ULAS_PATHMAX);
      break;
    case 'l':
      cfg->lst_path = strndup(optarg, ULAS_PATHMAX);
      break;
    case 'I':
      assert(incpathslen < ULAS_INCPATHSMAX);
      incpaths[incpathslen++] = strndup(optarg, ULAS_PATHMAX);
      break;
    case 'a':
      cfg->org = strtol(optarg, NULL, 0);
      break;
    case 'w':
      cfg->warn_level ^= warnings[(int)optarg[0]];
      break;
    case 'd':
      cfg->disas = 1;
      break;
    case 'A':
      cfg->print_addrs = 1;
      break;
    case 'D':
      assert(defslen < ULAS_DEFSMAX);
      defs[defslen++] = strndup(optarg, ULAS_PATHMAX);
      break;
    case 'S':
      if (strcmp("ulas", optarg) == 0) {
        cfg->sym_fmt = ULAS_SYM_FMT_DEFAULT; 
      } else if (strcmp("mlb", optarg) == 0) {
        cfg->sym_fmt = ULAS_SYM_FMT_MLB; 
      } else if (strcmp("sym", optarg) == 0) {
        cfg->sym_fmt = ULAS_SYM_FMT_SYM;
      }else {
        printf("Invalid symbol format: %s\n", optarg);
        exit(-1);
      }
      break;
    case '?':
      break;
    default:
      printf("%s: invalid option '%c'\nTry '%s -h' for more information.\n",
             ULAS_NAME, c, ULAS_NAME);
      exit(-1);
      break;
    }
  }

  cfg->argc = argc - optind;
  cfg->argv = argv + optind;
}

int main(int argc, char **argv) {
  // map args to cfg here
  struct ulas_config cfg = ulas_cfg_from_env();

  ulas_getopt(argc, argv, &cfg);
  cfg.incpaths = incpaths;
  cfg.incpathslen = incpathslen;
  cfg.defs = defs;
  cfg.defslen = defslen;

  int res = ulas_main(cfg);

  if (cfg.output_path) {
    free(cfg.output_path);
  }

  if (cfg.sym_path) {
    free(cfg.sym_path);
  }

  if (cfg.lst_path) {
    free(cfg.lst_path);
  }

  for (int i = 0; i < incpathslen; i++) {
    free(incpaths[i]);
  }

  return res;
}
