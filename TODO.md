# TODO

- Add cycle count to each instruction and output it to the listing.
- Add label size to listing and symbol files. Labels should have the size of `current_label` - `previous_label` unless .org was used.
- Add `#if` preprocessor directive that can evaluate expressions
- Add `.error` and `.warning` directives 
- Add failing tests for most failure cases
- Assert `.section` in full test
- Add `.bank.` directive allowing users to set the current bank number (ulas.bank) which can be used for symbol files.
