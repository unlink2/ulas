NAME=ulas
TEST_NAME=test$(NAME)
DBGCFLAGS=-g -fsanitize=address
DBGLDFLAGS=-fsanitize=address 
CFLAGS=-Wall -pedantic $(DBGCFLAGS) -std=gnu99
LIBS=
LDFLAGS=$(DBGLDFLAGS) $(LIBS)

INSTALL_DIR=/usr/local
OBJS:=ulas.o archs.o uldas.o 

CCOBJ=$(CC) -c -o $@ $< $(CFLAGS) $(LDFLAGS)

all: bin test

release: 
	make DBGCFLAGS="" DBGLDFLAGS=""


main.o: src/main.c
	$(CCOBJ)
test.o: src/test.c
	$(CCOBJ)
ulas.o: src/ulas.c
	$(CCOBJ)
uldas.o: src/uldas.c
	$(CCOBJ)
archs.o: src/archs.c
	$(CCOBJ)
	

bin: main.o $(OBJS) 
	$(CC) -o $(NAME) main.o $(OBJS) $(CFLAGS) $(LDFLAGS)

test: test.o $(OBJS) 
	$(CC) -o $(TEST_NAME) test.o $(OBJS) $(CFLAGS) $(LDFLAGS)

.PHONY: clean
clean:
	rm -f ./*.o
	rm -f ./$(NAME)
	rm -f ./$(TEST_NAME)

.PHONY: install 
install:
	mkdir -p $(INSTALL_DIR)/bin
	mkdir -p $(INSTALL_DIR)/man/man1
	mkdir -p $(INSTALL_DIR)/man/man5
	cp ./$(NAME) $(INSTALL_DIR)/bin
	cp ./man/$(NAME).1 $(INSTALL_DIR)/man/man1
	cp ./man/$(NAME).5 $(INSTALL_DIR)/man/man5

.PHONY: tags 
tags:
	ctags --recurse=yes --exclude=.git --extras=*  --fields=*  --c-kinds=* --language-force=C  .

.PHONY: ccmds
ccmds:
	bear -- make SHELL="sh -x -e" --always-make

.PHONY: format
format:
	clang-format -i ./src/*.c ./src/*.h

.PHONY: lint 
lint:
	clang-tidy ./src/*.h ./src/*.c

buildtests: bin 
	./$(NAME) tests/t0.s -D ULAS_PREDEF -l - -o tests/t0.bin
	./$(NAME) tests/t0.bin -d - -o tests/t0_dasm.s
